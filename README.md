# Time spend watching memes on youtube

A python script to find out how long you watched a youtube playlist for.

## Getting started

This script is made to run in a Docker container. You can run the Python file as is but some of the longer playlists take a long time to process.

### Prerequisites

The only prerequisite is that you have Docker installed.

If you don't want to use Docker you need to use Python 3 and pip install youtube_dl.

### Instalation

Run the folowing command to create the Docker images.
``` 
docker build -t python37-ytdl:latest -t python37-ytdl:1.0 . 
```
Now you are ready to use the image you created. There are 2 ways to run the script. 
The first one is for smaller playlists. It will automaticly remove the container after is is done.
```
docker run --rm python37-ytdl:latest <link> [number of videos]
```
The second one is for larger playlists. It will run the container in detatched mode meaning you wont see the output when it is done and it won't automaticly delete itself. The advantage of runnning the container this way is you won't have to wait for it to finish. 
``` 
docker run -d python37-ytdl:latest <link> [number of videos]  
```
To view the output you will need to check the logs of the container. You can find the container name by using the folowing command.
```
docker ps -a
```
Then you can use the name in the following command to vieuw the output.
```
docker logs containername
```
Afterwards you can delete the container using this command.
```
docker rm containername
```
